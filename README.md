    ## Quarkus
The process to build and run the Quarkus application is very similar to the Spring Boot one. First, to create the native image, you also need either the GRAALVM installed and the `GRAALVM_HOME` env variable set, or we can use docker to build the native image.

To build the native version locally, run the command:
```
./mvnw package -Pnative -f pom.xml
```
Or this one to build using docker:
```
./mvnw package -Pnative -Dquarkus.native.container-build=true -f pom.xml
```
And to the JVM version:
```
./mvnw package -f pom.xml
```

To start the application locally, use either the scripts `start_app.sh` and `start_jvm.sh` with the docker DB:
```
docker run -e POSTGRES_PASSWORD=example -p 5432:5432 postgres
```
Or use the script to build the docker image of the application, running:
```bash
./build.sh

## script content
## ./mvnw quarkus:add-extension -Dextensions=container-image-docker
## ./mvnw package -Dquarkus.container-build=true -f pom.xml &&
## docker build -f src/main/docker/Dockerfile.jvm -t quarkus-project:0.1-SNAPSHOT .
```
To build the docker image of the JVM version, and running the following command to the native version:
```bash
./build.sh native

## script content
## ./mvnw quarkus:add-extension -Dextensions=container-image-docker
## ./mvnw package -Pnative -Dquarkus.native.container-build=true -f pom.xml &&
## docker build -f src/main/docker/Dockerfile.native -t quarkus-project:0.1-SNAPSHOT .
```
Then, once again, you can also run both application and DB from docker, using:
```
docker-compose -f src/main/docker/quarkus.yml up
```

Now you have all you need to reproduce the tests with your machine.

### Relevant Articles:

- [Spring Boot vs Quarkus](https://www.baeldung.com/spring-boot-vs-quarkus)
